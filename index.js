console.log("Hello world!")
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
function credentials(){
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");
	let location = prompt("Where do you live?");
	alert("Thank you for your input");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old");
	console.log("You live in " + location);
}

credentials();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printBands(){
	let favBands = ["The beatles", "Metallica", "The eagles", "L'arc~en~Ciel", "Eraserheads"];
	console.log("1. " + favBands[0]);
	console.log("2. " + favBands[1]);
	console.log("3. " + favBands[2]);
	console.log("4. " + favBands[3]);
	console.log("5. " + favBands[4]);
}
printBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
function printMovies(){
	let favMovies = ["Taare zameen par", "Zindagi na milegi dobara", "3 idiots", "Big hero 6", "The silence of the lambs"];
	let inRottenTomatoes = ["92%", "93%", "100%", "90%", "95%"];
	console.log("1. " + favMovies[0])
	console.log("Rotten tomatoes ratings: " + inRottenTomatoes[0])
	console.log("2. " + favMovies[1])
	console.log("Rotten tomatoes ratings: " + inRottenTomatoes[1])
	console.log("3. " + favMovies[2])
	console.log("Rotten tomatoes ratings: " + inRottenTomatoes[2])
	console.log("4. " + favMovies[3])
	console.log("Rotten tomatoes ratings: " + inRottenTomatoes[3])
	console.log("5. " + favMovies[4])
	console.log("Rotten tomatoes ratings: " + inRottenTomatoes[4])
}

printMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();